import re
import os
import sys
import logging
import requests

import config
import twitter
from praw import Reddit
from vk import set_access_token, get_group
from vk.photos import Photo
from random import choice, shuffle

# from utils import get_reddit_attachment

LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, stream=sys.stdout)

hot_limit = 20
subreddit = 'ProgrammerHumor'
pattern = re.compile(r"(jpe?g|png)|imgur")
prefix = '#ithumor@lambdait'


def is_image_file(attachment_url):
    _, file_extension = os.path.splitext(attachment_url)

    if file_extension not in ('.jpg', '.gif', '.png'):
        return False

    return True


def download_attachment(attachment_url):
    response = requests.get(attachment_url, stream=True)
    return response.content


def get_reddit_attachment(post):
    binary_content = download_attachment(post.url)
    _, filename = os.path.split(post.url)
    yield (filename, binary_content)


if __name__ == '__main__':
    # set up logger
    logger = logging.getLogger()
    
    # auth in reddit
    reddit = Reddit('bot1')

    # auth in VK
    set_access_token(config.VK_ACCESS_TOKEN)
    group = get_group(config.VK_GROUP)

    # auth in twitter
    twitter_api = twitter.Api(consumer_key=config.TWITTER_CONSUMER_KEY, 
        consumer_secret=config.TWITTER_CONSUMER_SECRET, 
        access_token_key=config.TWITTER_ACCESS_TOKEN_KEY, 
        access_token_secret=config.TWITTER_ACCESS_TOKEN_SECRET)

    # get top posts from subreddit
    subreddit = reddit.subreddit(subreddit)
    hot = list(subreddit.hot(limit=hot_limit))

    while len(hot) > 0:
        shuffle(hot)
        post = hot.pop()

        if re.findall(pattern, post.url):
            attachment_items = {filename: binary_content for filename, 
                binary_content in get_reddit_attachment(post)}

            # post to vk
            photo_items = Photo.upload_wall_photos_for_group(int(config.VK_GROUP),
                attachment_items.items())
            group.wall_post(message='{0}\n\n{1}'.format(prefix, post.title),
                attachments=photo_items)
            logger.info('post has been successfully published to vk')

            # post to twitter
            status = '{0}\n\n{1}'.format(prefix, post.title)
            media = post.url
            twitter_api.PostMedia(status=status, media=media)
            logger.info('post has been successfully published to twitter')

            break
