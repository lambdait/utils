import os
import sys
import datetime
import logging
import requests
import textwrap

import config
import trello
import twitter
import vk
from vk.photos import Photo

# from utils import get_attachment_in_card, is_card_can_published

LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'
logging.basicConfig(level=logging.INFO, stream=sys.stdout)


def is_image_file(attachment_url):
    _, file_extension = os.path.splitext(attachment_url)

    if file_extension not in ('.jpg', '.gif', '.png'):
        return False

    return True


def download_attachment(attachment_url):
    response = requests.get(attachment_url, stream=True)
    return response.content


def get_attachment_in_card(card):
    attachment_items = (attachment for attachment in card.get_attachments() if is_image_file(attachment.url))

    for attachment in attachment_items:
        binary_content = download_attachment(attachment.url)
        _, filename = os.path.split(attachment.url)
        yield (filename, binary_content)


def is_card_can_published(trello_card):
    if not trello_card.due_date:
        return False

    now_unixtime = datetime.datetime.utcnow().replace(tzinfo=None).timestamp()
    card_due_unixtime = trello_card.due_date.replace(tzinfo=None).timestamp()

    delta = card_due_unixtime - now_unixtime
    return True if delta <= 0 else False


if __name__ == '__main__':
    # set up logger
    logger = logging.getLogger()

    # auth in vk
    vk.set_access_token(config.VK_ACCESS_TOKEN)
    group = vk.get_group(config.VK_GROUP)

    # auth in twitter
    twitter_api = twitter.Api(consumer_key=config.TWITTER_CONSUMER_KEY, 
        consumer_secret=config.TWITTER_CONSUMER_SECRET, 
        access_token_key=config.TWITTER_ACCESS_TOKEN_KEY, 
        access_token_secret=config.TWITTER_ACCESS_TOKEN_SECRET)

    # auth in trello
    trello_api = trello.TrelloClient(api_key=config.TRELLO_KEY, token=config.TRELLO_TOKEN)

    # get working board
    board = trello_api.get_board(config.TRELLO_BOARD)

    card_items = (card for card in board.open_cards() if is_card_can_published(card))
    logger.info('board cards retrieved')

    for card in card_items:
        attachment_items = \
            {filename: binary_content for filename, binary_content in get_attachment_in_card(card)}
        
        # post to vk
        try:
            photo_items = Photo.upload_wall_photos_for_group(group.id, attachment_items.items())
            group.wall_post(message=card.name + '\n' + card.description, attachments=photo_items)
            logger.info('post has been successfully published to vk')
        except vk.error.VKParseJsonError as e:
            logger.error('vk posting error:', e)
            raise e

        # post to twitter
        try:
            message = card.name + '\n' + textwrap.shorten(card.description, width=130)
            media_list = [media.url for media in card.get_attachments() if is_image_file(media.url)]
            media_url = media_list[0]
            if attachment_items:
                twitter_api.PostMedia(status=message, media=media_url)
            logger.info('post has been successfully published to twitter')
        except Exception as e:
            logger.error('twitter posting error:', e)
            raise e
        
        card.set_due_complete()
        card.set_closed(True)
